var gulp = require('gulp');

var nodemon = require('gulp-nodemon');

gulp.task('copyJs', function() {
    gulp.src(['./src/**/*.js'])
        .pipe(gulp.dest('./build/'));
});

gulp.task('runDownloadFeed', ['copyJs'], function() {
    var nm = nodemon({
        script: 'build/runDownloadFeed.js',
        ext: 'js html'
    })
    .on('exit', function () {
        process.exit();
    });
});