var request = require('request'),
sinon = require('sinon'),
stream = require('stream'),
fs = require('fs'),
assert = require("assert"),
downloadFeed = require('../feed/downloadFeed.js');

describe('Download feed', function() {
    before(function(done) {
        var fakeRequest = function (url) {
            var output = new stream.Readable();

            var data = fs.readFileSync(__dirname + '/testDownloadFeed.xml', "utf8");

            output.push(data);
            output.push(null);

            return output;
        };
        sinon.stub(request, 'get', fakeRequest);
        done();
    });

    after(function(done){
        request.get.restore();
        done();
    });


    it('parse is ok', function(done) {
        var result = [];
        downloadFeed.pipeFeed("https://test", ['ean', 'url', 'title', 'description', 'price', 'price_shipping'], function(data) {
            result.push(data);
        },
        function() {
            assert.equal(2, result.length);
            assert.equal('8718807129399', result[0].ean);
            assert.equal('9,95', result[0].price);
            assert.equal('8718807356511', result[1].ean);
            assert.equal('15,95', result[1].price);
            done();
        });
    });
});