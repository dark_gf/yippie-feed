var mongodb = require('mongodb');
var config = require('config');
var log4js = require('log4js');
var logger = log4js.getLogger();


var MongoClient = require('mongodb').MongoClient;

MongoClient.connect(config.get("mongo").url, function(err, db) {
    if (err) throw err;

    var collection = db.collection('feed');

    var query = collection.initializeUnorderedBulkOp();
    var count = 0;
    var bulkMaxSize = 1000;
    var totalCount = 0;

    var downloadFeed = require('./feed/downloadFeed.js');

    downloadFeed.pipeFeed("https://storage.googleapis.com/yippie-samples/feed.xml", ['ean', 'url', 'title', 'description', 'price', 'price_shipping'], function(data) {
        var price = parseFloat(data.price.replace(',', '.'));
        var priceShipping = parseFloat(data.price_shipping.replace(',', '.'));
        if (price) {
            data.price = price;
            data.price_shipping = priceShipping;
            data._id = data.ean;
            delete data.ean;

            query.find({_id: data._id, price: {$gt: data.price}}).upsert().replaceOne(data);
            count++;
            totalCount++;
            if (count >= bulkMaxSize) {
                query.execute();
                logger.debug('Parsed ' + totalCount + ' objects');
                query = collection.initializeUnorderedBulkOp();
                count = 0;
            }

        }
    }, function() {
        query.execute();
        logger.debug('Total ' + totalCount + ' objects');
        db.close();
        logger.info('Done');
    });

});




