var XmlStream = require('xml-stream'),
fs = require('fs'),
log4js = require('log4js'),
logger = log4js.getLogger(),
request = require('request'),
http = require('http'),
Stream = require('stream').Stream;
_ = require('lodash');


/**
 * Streams xml from ulr and parse it on the fly, on every found product callback
 * is called, callbackEnd executed when parse is done
 *
 * @param String url
 * @param Array extractColumns - columns that should be extracted from xml feed
 * @param function callback
 * @param function callbackEnd
 */
function pipeFeed(url, extractColumns, callback, callbackEnd) {
    var stream = new Stream();

    stream.writable = true;

    stream.write = function(data) {
        stream.emit("data", data);
    }

    stream.end = function() {
        callbackEnd();
        stream.emit("end");
    }

    stream.destroy = function() {
        stream.emit("close");
    }

    var xml = new XmlStream(stream);
    xml.preserve('record', true);
    xml.collect('column');
    xml.on('endElement: record', function(item) {
        var result = {};
        _.forEach(item.column, function(column) {
            var name = _.get(column, '$.name');
            if (name && extractColumns.indexOf(name) > -1) {
                var value = column['$text'];
                result[name] = value;
            }
        });
        callback(result);
    });

    request.get(url)
    .on('response', function(response) {
        logger.debug('Response code for ' + url + ' : ' + response.statusCode);
    })
    .pipe(stream);
}

module.exports = {"pipeFeed": pipeFeed};